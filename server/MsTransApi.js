let axios = require('axios')
let { parseString } = require('xml2js')

class MsTransApi {

    /**
     * Create an instance of MsTransApi
     * @param {string} apiKey - api key for ms translator text
     */
    constructor(apiKey) {

        let mAxios = axios.create({
            headers: {
                'Ocp-Apim-Subscription-Key': apiKey
            },
            params: {
                from: 'ja',
                to: 'vi'
            },
            responseType: 'document',
            baseURL: 'https://api.microsofttranslator.com/V2/Http.svc'
        })

        this.translate = async function (text) {
            let res = await mAxios.get('Translate', {
                params: { text }
            })
            let result = await new Promise((resolve, reject) => {
                parseString(res.data, (err, result) => {
                    resolve(result.string._)
                })
            })
            return result
        }
    }
}

module.exports = MsTransApi

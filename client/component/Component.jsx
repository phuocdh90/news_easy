import React from 'react'

export default class Component extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <div>
                Component
            </div>
        )
    }
}

Component.defaultProps = {
}

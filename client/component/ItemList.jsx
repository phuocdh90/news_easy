import React from 'react'
import ItemCard from './ItemCard.jsx'

export default class ItemList extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <div className="row">
                {this.props.data.map((item, i) => {
                    return <ItemCard key={item.news_id} itemData={item} theme={i ? 'light' : 'dark'} />
                })}
            </div>
        )
    }
}

ItemList.defaultProps = {
    data: []
}

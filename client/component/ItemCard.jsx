import React from 'react'

import { Link } from 'react-router-dom'

export default class ItemCard extends React.Component {

    constructor(props) {
        super(props)

        this.boxColorSchemes = [
            'text-white bg-primary',
            'text-white bg-secondary',
            'text-white bg-success',
            'text-white bg-danger',
            'text-white bg-info',
            'bg-light',
            'text-white bg-dark',
        ]
    }

    render() {
        let { itemData, theme } = this.props

        return (
            <div className="col-lg-6">
                <div className={`card bg-${theme} ${theme !== 'light' ? 'text-white' : ''}`}>
                    <img src={itemData.news_web_image_uri} alt="{No image}" className="card-img-top" />
                    <div className="card-body">
                        <h4 className="card-title" dangerouslySetInnerHTML={{ __html: itemData.title_with_ruby }} />
                        <h6 className="mb-2 mt-2 text-muted">{itemData.news_prearranged_time}</h6>
                        <div className="card-text" dangerouslySetInnerHTML={{ __html: itemData.outline_with_ruby }} />
                        <Link to={`/news/${itemData.news_id}`} className={`btn btn-${theme !== 'light' ? 'light' : 'dark'}`}>
                            {'Read more'}
                        </Link>
                    </div>
                </div>
            </div>
        )
    }
}

ItemCard.defaultProps = {
}

import axios from 'axios'

class TransApi {

    constructor() {

        let mAxios = axios.create({
            params: {
                engine: 'ms'
            },
            baseURL: '/news_easy/trans'
        })

        this.translate = async function (text) {
            let res = await mAxios.get('', {
                params: { text }
            })
            return res.data
        }
    }
}

export default new TransApi()

import axios from 'axios'

class NewsApi {

    constructor() {

        let mBaseUrl = 'https://www3.nhk.or.jp/news/easy/'

        let mAxios = axios.create({
            baseURL: mBaseUrl
        })

        let mUrls = {
            topList: 'top-list.json'
        }

        this.getNewsList = async function (time) {
            if (!time) {
                time = new Date().getTime().toString()
            }

            let result = await mAxios.get(mUrls.topList, {
                params: {
                    '_': time
                }
            })

            if (result.status === 200) {
                result.data.forEach((item) => {
                    if (!item.news_web_image_uri) {
                        item.news_web_image_uri = `${mBaseUrl}${item.news_id}/${item.news_id}.jpg`
                    }
                    // create full path to movie
                    if (item.news_web_movie_uri) {
                        item.news_web_movie_uri = `rtmp://flv.nhk.or.jp/ondemand/flv/news/&movie=${item.news_web_movie_uri}`
                    }
                })
                return result.data
            }

            return []
        }

        this.getDictionary = async function (n_id, time) {
            if (!time) {
                time = new Date().getTime().toString()
            }

            let result = await mAxios.get(`${n_id}/${n_id}.out.dic`, {
                params: {
                    'date': time
                }
            })

            if (result.status === 200) {
                return result.data
            }

            return []
        }

        this.getArticle = async function (n_id) {
            let result = await mAxios.get(`${n_id}/${n_id}.html`)

            if (result.status === 200) {
                let parser = new DOMParser()
                var serializer = new XMLSerializer()
                let htmlDoc = parser.parseFromString(result.data, 'text/html')

                let regularLink = htmlDoc.getElementById('js-regular-news').getAttribute('href')
                let content = serializer.serializeToString(htmlDoc.getElementById('js-article-body'))
                return {
                    regularLink,
                    content,
                    audioLink: `${mBaseUrl}${n_id}/${n_id}.mp3` // currently broken
                }
            }

            return {}
        }
    }
}

export default new NewsApi()

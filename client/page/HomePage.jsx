import React from 'react'

import store from '../redux/Store'
import * as ActionCreator from '../redux/ActionCreator'

import ItemListContainer from '../container/ItemListContainer'

export default class HomePage extends React.Component {

    constructor(props) {
        super(props)

        store.dispatch(ActionCreator.fetchTopList())
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <div>
                <ItemListContainer />
            </div>
        )
    }
}

import React from 'react'

import store from '../redux/Store'
import * as ActionCreator from '../redux/ActionCreator'

import NewsApi from '../api/NewsApi'
import TransApi from '../api/TransApi'

export default class ArticlePage extends React.Component {

    constructor(props) {
        super(props)

        this.onBackClick = this.onBackClick.bind(this)
    }

    componentWillMount() {
        if (!this.props.meta) {
            store.dispatch(ActionCreator.fetchTopList())
        }
        store.dispatch(ActionCreator.fetchItem(this.props.newsId))
    }

    componentDidMount() {
    }

    componentDidUpdate() {
        let { dict } = this.props
        if (!dict) {
            return
        }
        let ids = Object.keys(dict)
        ids.forEach(async id => {
            // prepare data
            let rawData = dict[id]
            let title = rawData[0].hyouki
            let data
            // combine results
            if (rawData.length > 1) {
                data = rawData.map((item, i) => {
                    return `<p><b>${i + 1}. </b>${item.def}</p>`
                }).join('')
            } else {
                data = rawData[0].def + '<p/>'
            }
            // additional translation
            // let trans = await TransApi.translate(rawData[0].hyouki[0])
            // data += `<small><i>Dịch tự động</i></small><br/><p>${trans.translated}</p>`
            // enable popover
            $(`*[id=id-${id}]`).each(function (index) {
                // insert data into element
                $(this).attr('data-placement', 'top')
                    .attr('data-template', ArticlePage.POPOVER_TEMPLATE)
                    .attr('data-container', 'body')
                    .attr('title', title)
                    .attr('data-content', data)
                    .attr('data-html', 'true')
                    .attr('tabindex', '0')
            }).popover({
                // close popover on lose focus
                trigger: 'focus'
            })
        })
    }

    componentWillUnmount() {
    }

    onBackClick() {
        this.props.history.push('/')
    }

    render() {
        let { meta, content } = this.props
        return [
            <p key='spacing'></p>,
            <div key='top buttons' className="row mb-3">
                <div className="col">
                    <button type="button" className="btn btn-primary" onClick={this.onBackClick}>
                        {'Home'}
                    </button>
                </div>
                <div className="col">
                    {content ?
                        <a className="btn btn-primary float-right" href={content.regularLink} target="_blank" role="button">Full article</a>
                        :
                        null
                    }
                </div>
            </div>,
            <div key='title' className="row mb-3">
                {meta ?
                    <div>
                        <h4 className="col-12" dangerouslySetInnerHTML={{ __html: meta.title_with_ruby }} />
                        <img className="col-12" src={meta.news_web_image_uri} />
                        {/* The code bellow will not work because html5 video tag does not support rtmp (flash) protocol */}
                        {/*meta.news_web_movie_uri ?
                            <video className="col-12" controls>
                                <source src={meta.news_web_movie_uri} type={`video/${meta.news_web_movie_uri.substr(meta.news_web_movie_uri.length - 3)}`} />
                                <span>Your browser does not support the video tag.</span>
                            </video>
                            :
                            <img className="col-12" src={meta.news_web_image_uri} />
                        */}
                    </div>
                    :
                    <div className="col">Loading metadata...</div>
                }
            </div>,
            <div key='content' className="row">
                {content ?
                    <div>
                        <div className="col-12">
                            <audio src={content.audioLink} controls>{'Audio Tag not supported...'}</audio>
                        </div>
                        <div className="col-12" dangerouslySetInnerHTML={{ __html: content.content }} />
                    </div>
                    :
                    <div className="col">Loading content...</div>
                }
            </div>
        ]
    }
}

ArticlePage.POPOVER_TEMPLATE = `<div class="popover bg-dark" role="tooltip">
                        <div class="arrow"></div>
                        <h3 class="popover-header"></h3>
                        <div class="popover-body text-white"></div>
                    </div>`

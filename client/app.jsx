import React from 'react'
import ReactDOM from 'react-dom'

import 'babel-polyfill'

import MainLayout from './MainLayout.jsx'

ReactDOM.render((
    <MainLayout />
), document.getElementById('react-root'))

import React from 'react'
import { Provider } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import store from './redux/Store'

import HomePage from './page/HomePage.jsx'
import ArticlePageContainer from './container/ArticlePageContainer'

export default class MainLayout extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
    }

    componentWillUnmount() {
    }

    render() {
        return (
            <Provider store={store}>
                <Router basename='news_easy'>
                    <div className='container'>
                        <Switch>
                            <Route exact path='/' component={HomePage} />
                            <Route path='/news/:news_id' component={ArticlePageContainer} />
                        </Switch>
                    </div>
                </Router>
            </Provider>
        )
    }
}

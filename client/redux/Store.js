import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import reducer from './Reducer'
import mySaga from './Saga'

// create the saga middleware
const sagaMiddleware = createSagaMiddleware()

// create store
let store = createStore(reducer, applyMiddleware(sagaMiddleware))

// run the saga
sagaMiddleware.run(mySaga)

export default store

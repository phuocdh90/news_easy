import * as ActionType from './ActionType'

let updateData = function (newArray, dataList = [], key = 'id') {
    let dataListNew = {}
    let idList = newArray.map((element) => {
        dataListNew[element[key]] = element
        return element[key]
    })
    Object.assign({}, dataList, dataListNew)
    return { idList, dataListNew }
}

let initialState = {
    newsById: [],
    topList: [],
    newsDetailById: [],
    topListFetching: false
}

export default function reducer(state = initialState, action) {

    switch (action.type) {
        case ActionType.TOP_LIST_FETCH: {
            return Object.assign({}, state, {
                topListFetching: true
            })
        }
        case ActionType.TOP_LIST_FETCH_SUCCEEDED: {
            let { idList, dataListNew } = updateData(action.topList, state.newsById, 'news_id')
            return Object.assign({}, state, {
                newsById: dataListNew,
                topList: idList,
                topListFetching: false
            })
        }
        case ActionType.TOP_LIST_FETCH_FAILED: {
            return Object.assign({}, state, {
                topListFetching: false
            })
        }
        case ActionType.ITEM_FETCH: {
            return state
        }
        case ActionType.ITEM_FETCH_SUCCEEDED: {
            let { idList, dataListNew } = updateData([action.item], state.newsDetailById, 'id')
            return Object.assign({}, state, {
                newsDetailById: dataListNew,
                itemFetching: false
            })
        }
        case ActionType.ITEM_FETCH_FAILED: {
            return state
        }
        default:
            return state
    }
}

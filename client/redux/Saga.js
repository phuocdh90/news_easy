import { all, call, put, takeLatest } from 'redux-saga/effects'

import * as ActionType from './ActionType'
import * as ActionCreator from './ActionCreator'
import NewsApi from '../api/NewsApi'

/**
 * worker Saga: will be fired on FETCH_TOP_LIST actions
 * @param {object} action - FETCH_TOP_LIST action
 */
function* fetchTopList(action) {
    try {
        const topList = yield call(NewsApi.getNewsList, action.time)
        yield put(ActionCreator.fetchTopListSucceeded(topList))
    } catch (e) {
        yield put(ActionCreator.fetchTopListFailed(e.message))
    }
}

/**
 * worker Saga: will be fired on ITEM_FETCH actions
 * @param {object} action - ITEM_FETCH action
 */
function* fetchItem(action) {
    try {
        const content = yield call(NewsApi.getArticle, action.itemId)
        const dict = (yield call(NewsApi.getDictionary, action.itemId)).reikai.entries
        yield put(ActionCreator.fetchItemSucceeded({ id: action.itemId, content, dict }))
    } catch (e) {
        yield put(ActionCreator.fetchItemFailed(e.message))
    }
}

/**
 * "takeLatest" does not allow concurrent fetches of news list. If an event gets
 * dispatched while a fetch is already pending, that pending fetch is cancelled
 * and only the latest one will be run.
 */
function* mySaga() {
    yield all([
        takeLatest(ActionType.TOP_LIST_FETCH, fetchTopList),
        takeLatest(ActionType.ITEM_FETCH, fetchItem)
    ])
}

export default mySaga


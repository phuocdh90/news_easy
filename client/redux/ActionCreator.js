import * as ActionType from './ActionType'

export function fetchTopList(time) {
    return {
        type: ActionType.TOP_LIST_FETCH,
        time
    }
}

export function fetchTopListSucceeded(topList) {
    return {
        type: ActionType.TOP_LIST_FETCH_SUCCEEDED,
        topList
    }
}

export function fetchTopListFailed(message) {
    return {
        type: ActionType.TOP_LIST_FETCH_FAILED,
        message
    }
}

export function fetchItem(itemId) {
    return {
        type: ActionType.ITEM_FETCH,
        itemId
    }
}

export function fetchItemSucceeded(item) {
    return {
        type: ActionType.ITEM_FETCH_SUCCEEDED,
        item
    }
}

export function fetchItemFailed(message) {
    return {
        type: ActionType.ITEM_FETCH_FAILED,
        message
    }
}

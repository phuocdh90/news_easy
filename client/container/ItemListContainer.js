import { connect } from 'react-redux'
import { fetchItem } from '../redux/ActionCreator'
import ItemList from '../component/ItemList.jsx'

const mapStateToProps = state => {
    return {
        data: state.topList.map((id) => {
            return state.newsById[id]
        })
    }
}

const mapDispatchToProps = dispatch => {
    return {
        // onItemClick: id => {
        //     dispatch(fetchItem(id))
        // }
    }
}

const ItemListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(ItemList)

export default ItemListContainer

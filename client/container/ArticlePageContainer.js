import { connect } from 'react-redux'
import { fetchTopList } from '../redux/ActionCreator'
import ArticlePage from '../page/ArticlePage.jsx'

const mapStateToProps = (state, ownProps) => {
    let newsId = ownProps.match.params.news_id
    let details = state.newsDetailById[newsId]
    return {
        newsId,
        meta: state.newsById[newsId],
        content: details ? details.content : null,
        dict: details ? details.dict : null
    }
}

const ArticlePageContainer = connect(
    mapStateToProps
)(ArticlePage)

export default ArticlePageContainer

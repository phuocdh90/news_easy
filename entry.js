class RubikApp {

    constructor() {
        let ejs = require('ejs');
        let crypto = require('crypto');
        let fs = require('fs');

        let MsTransApi = require('./server/MsTransApi')
        let trans = new MsTransApi('a10372e21a534374891a5a0fcc97c0d3')

        let checksum = function (str, algorithm, encoding) {
            return crypto
                .createHash(algorithm || 'md5')
                .update(str, 'utf8')
                .digest(encoding || 'hex');
        }

        let calculateFileChecksum = function (filePath, callback) {
            let data = fs.readFileSync(filePath);
            let res = null;
            if (data) {
                res = checksum(data);
            }
            return res;
        }

        this.init = function (rootPath, fileRoot) {
            this.rootPath = rootPath;
            this.fileRoot = fileRoot;

            // templates
            this.indexPage = ejs.compile(fs.readFileSync(`${this.rootPath}index.ejs`, 'utf8'))
        }

        this.handleRoot = function (req, res) {
            let bundle_checksum = calculateFileChecksum(this.rootPath + 'public/build/bundle.js');
            let css_main_checksum = calculateFileChecksum(this.rootPath + 'public/css/main.css');

            res.send(this.indexPage({
                root_path: this.fileRoot + '/',
                bundle_param: (bundle_checksum != null ? '?md5=' + bundle_checksum : ''),
                css_main_param: (css_main_checksum != null ? '?md5=' + css_main_checksum : '')
            }));
        }

        this.description = function () {
            return {
                name: 'News',
                icon: '/image/icon.svg'
            };
        };

        this.handleRoot = this.handleRoot.bind(this);
        this.description = this.description.bind(this);

        /* Additional controllers */
        this.trans = async function (req, res) {
            let { text } = req.query
            if (!text || !text.trim()) {
                res.json({
                    status: 400,
                    message: 'parameter "text" is required'
                })
                return
            } else {
                text = text.trim()
            }
            let result = await trans.translate(text)
            res.json({
                translated: result
            })
        }

        this.paths = [{
            method: 'get',
            path: '/trans',
            handler: this.trans
        }, {
            method: 'get',
            path: '/',
            handler: this.handleRoot
        }, {
            method: 'get',
            path: '/*',
            handler: this.handleRoot
        }];
    }
}

module.exports = exports = new RubikApp();
